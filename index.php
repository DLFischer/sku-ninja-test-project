<?php
// ******************************************
// Start the session
// ******************************************
if(!isset($_SESSION)) {
	session_start();
}

// Default timezone must be set or PHP 7.x will throw a warning
date_default_timezone_set('America/Chicago');


// ******************************************
// Initialize variables to hold errors and other messages
// ******************************************
if(!isset($errors)) {
	$errors = array();
}
if(!isset($_SESSION['user_message'])) {
	$_SESSION['user_message'] = '';
}


// ******************************************
// Only logged in Admins get to view this page, right?  :)
// ******************************************
if(!isset($_SESSION['AccessLevel'])) {
	$_SESSION['AccessLevel'] = '';
}
$access_is_allowed = false;
if($_SESSION['AccessLevel'] == 'WSAdmin') {
	$access_is_allowed = true;
}


// Log out a user and clear all session variables
if(isset($_POST['do_logout'])) {
	foreach($_SESSION as $key => $value){
		$_SESSION[$key] = NULL;
		unset($_SESSION[$key]);
    }
	
	$_SESSION['user_message'] = '<div class="alert alert-success mt-4 mb-4">You have successfully logged off.</div>';
	session_write_close();
	header("Location: index.php");
	exit();
}


// Validate request to login to this site.
if((isset($_POST['Username'])) && (isset($_POST['Password']))) {

	$valid_user_found = false;
	$entered_username = trim($_POST['Username']);
	$entered_password = trim($_POST['Password']);
	
	if($entered_username == "" || $entered_password == "") {
		$errors[] = "Enter both your username and password.";
	}	
	
	if(empty($errors)) {
		if($entered_username == "WSAdmin001" && $entered_password == "WhtSpDrR0ckZ!2019") {
			$valid_user_found = true;
		} else {
			$errors[] = "Your login credentials are invalid.";
		}
	}

	if((empty($errors)) && ($valid_user_found == true)) {
		// Assign value to SESSION variable
		$_SESSION['AccessLevel'] = "WSAdmin"; 
		
		// Redirect
		session_write_close();
		header("Location: index.php");
		exit();
	}
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>WhyteSpyder SKU Ninja Event Log Viewer by Dorinda Fischer</title>
    <meta name="keywords" content="WhyteSypder, SKU Ninja, Dorinda Fischer, super awesome web developer">
    <meta name="description" content="Roger's little project for WhyteSpyder candidates">
		
    <!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
			  
	<!-- Bootstrap 4-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		
    <!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">  

    <!-- DataTables jQuery Plugin -->
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
	<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">  
    
	<script>
	$( document ).ready(function() {
		$('#log_table').DataTable();
	}); 
	</script>
	
	<style>
	.header_container {
		background-color: #12acc9;
		padding: 10px;
		margin-bottom: 20px;
	}
	.header_container img {
		width: 340px;
		max-width: 100%;
	}
	.header_container h1 {
		font-family: 'Oswald', sans-serif;
		color: #FFF;
		text-shadow: #343a40 1px 1px;
		margin-left: 15px;
	}
	.footer_container {
		color: #FFF;
		background-color: #12acc9;
		padding: 20px;
		margin-top: 30px;
	}
	#log_table {
		font-size: 0.9em;
	}
	.btn-info {
		background-color: #12acc9;
	}
	.page-link {
		color: #12acc9;
	}
	.page-link:hover {
		color: #0C768A;
	}	
	.page-item.active .page-link {
		color: #fff;
		background-color: #12acc9;
		border-color: #12acc9;
	}
	
	@media (min-width: 1200px) {
		.header_container {
			padding: 20px;
		}
		.header_container h1 {
			margin-top: 15px;
		}
	}
	</style>
</head>

<body>

<div class="header_container">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<img src="https://www.whytespyder.com/src/images/logo_lockup.png" alt="WhyteSpyder Sku Ninja Logo">
			</div>
			<div class="col-lg-6">
				<div class="text-xl-right">
					<h1>WhyteSpyder SKU Ninja Admin</h1>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">

	<?php if($access_is_allowed === true) { ?>
		<div class="row">
			<div class="col-md-7">
				<h2 class="pt-3 pb-2">Event Log Viewer</h2>
			</div>
			<div class="col-md-5">
				<div class="text-md-right pt-1 pt-md-4 pb-2">
					<form action="<?php echo htmlentities(strip_tags($_SERVER['PHP_SELF'])); ?>" method="post" name="logoutform1" id="logoutform1" class="form-horizontal">
						<input type="hidden" name="do_logout" value="yes">
						<button type="submit" name="logout" id="logout_button" class="btn btn-info" title="Log Out">Log Out</button>
					</form>
				</div>
			</div>
		</div>
		<hr class="d-md-none">
		<?php
		// Initialize some variables
		$json_content = '';
		$log_content_arr = array();
		
		// Get content from JSON file
		$json_input_file = 'SKUNinja-sample-logs.json';
		
		if(is_file($json_input_file)) {
			$json_content = file_get_contents('SKUNinja-sample-logs.json');
			
			// Create an associative array from the JSON content
			$log_content_arr = json_decode($json_content, true);
			
			if(empty($log_content_arr)) {
				$errors[] = 'No content was found.';
			}
			
		} else {
			$errors[] = 'The JSON source file could not be found.</div>';
		}
		
		
		// At this point, if the $errors variable is empty, we should have some some data to work with in $log_content_arr
		if(empty($errors)) {
			
			$count_normal = 0;
			$count_warnings = 0;
			$count_errors = 0;
			
			?>
				<?php
				// A variable to hold some HTML for our rows of results
				$results_html = '';
				
				foreach($log_content_arr as $log_item) {
					/*
					echo '<pre>';
					print_r($log_item);
					echo '</pre>';
					*/
					
					$found_type = '';
					if(isset($log_item['type'])) {
						$found_type = $log_item['type'];
					}
					
					$row_class = 'log_unknown';
					
					/*
					if($found_type == "1") {
						$row_class = 'log_normal';
					}
					if($found_type == "2") {
						$row_class = 'log_warning';
					}
					if($found_type == "3") {
						$row_class = 'log_error';
					}
					*/
					if($found_type == "1") {
						$row_class = 'text-white bg-success';
						$count_normal++;
					}
					if($found_type == "2") {
						$row_class = 'bg-warning';
						$count_warnings++;
					}
					if($found_type == "3") {
						$row_class = 'text-white bg-danger';
						$count_errors++;
					}
					$results_html .= '
					<tr class="'.$row_class.'">
						<td>
						';
						if(isset($log_item['created'])) { $results_html .= htmlentities($log_item['created']); } else { $results_html .= 'n/a'; } 
						$results_html .= '
						</td>
						<td>';
						if(isset($log_item['subject'])) { $results_html .= htmlentities($log_item['subject']); } else { $results_html .= 'n/a'; }
						$results_html .= '
						</td>
					</tr>';
					
				} // END foreach 
				
			if($results_html != "") {
			?>
				<div class="pt-3" style="font-size:0.9em;">
					<div class="row">
						<div class="col-12 col-md-3 col-lg-2">
							<div class="p-1 mb-1 text-left"><strong>Total Results:</strong></div>
						</div>
						<div class="col-7 col-md-3 col-lg-2">
							<div class="p-1 mb-1 text-center text-white bg-success"><?php echo $count_normal; ?> Normal</div>
						</div>
						<div class="col-7 col-md-3 col-lg-2">
							<div class="p-1 mb-1 text-center bg-warning"><?php echo $count_warnings; ?> Warnings</div>
						</div>
						<div class="col-7 col-md-3 col-lg-2">
							<div class="p-1 mb-1 text-center text-white bg-danger"><?php echo $count_errors; ?> Errors</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table" id="log_table">
					<thead class="thead-dark">
						<tr>
							<th>Date / Time</th>
							<th>Subject</th>
						</tr>
					</thead>
					<tbody>
						<?php echo $results_html; ?>
					</tbody>
				</table>
			<?php } else { ?>
				<p class="text-danger">There are no results to display.</p>
			<?php } ?>
		<?php	
		} // END if(empty($errors))
		?>
		
	<?php } ?>
	
	<?php
	// Display errors
	if(!empty($errors)) {
		echo '<div class="alert alert-danger mt-4 mb-4">
		<strong>ERROR:</strong> ';
		foreach($errors as $error_message) {
			echo '<div style="margin-bottom:3px;">- '.$error_message.'</div>';
		}
		echo '</div>';
	}
	?>
	<?php
	// Display other messages
	if((isset($_SESSION['user_message'])) && ($_SESSION['user_message'] != "")) {
		echo $_SESSION['user_message'];
		$_SESSION['user_message'] = NULL;
		unset($_SESSION['user_message']);
	}
	?>

	<?php if($access_is_allowed === false) { ?>
		
		<h2 class="pt-4 pb-4">LOG IN</h2>

		<div class="row">
			<div class="col-lg-5">
			
				<form action="<?php echo htmlentities(strip_tags($_SERVER['PHP_SELF'])); ?>" method="post" name="loginform1" id="loginform1" class="form-horizontal">
					
					<div class="form-group row">
						<div class="col-sm-4">
							<label for="Username"><strong>Username:</strong></label>
						</div>
						<div class="col-sm-8">
							<input name="Username" type="text" id="Username" class="form-control" placeholder="Enter your username" required>
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-sm-4">
							<label for="Username"><strong>Password:</strong></label>
						</div>
						<div class="col-sm-8">
							<input name="Password" type="password" class="form-control" id="Password" placeholder="Enter your password" required>
						</div>
					</div>

					<button type="submit" name="login_button" id="login_button" class="btn btn-info" title="Log In">Log In</button>
				</form>
	
			</div>
		</div>
		<p>&nbsp;</p>	

	<?php } ?>


	<p>&nbsp;</p>	

</div><!--container-->

<div class="footer_container">
	<div class="text-center">
		<div class="small">Event Log Viewer by Dorinda Fischer<br>
		Bootstrap v4.3.1 &#8226; jQuery v3.4.1 &#8226; DataTables jQuery plugin</div>
	</div>
</div>

</body>
</html>
